################################################################
#
# Vertec Database Restore Script
#
################################################################

$fb_gbak = "C:\VERTEC\Database\gbak.exe"
$fb_user = "SYSDBA"
$fb_pass = "masterkey"
$fb_host = "localhost"

$bkup_path = "C:\VERTEC_BACKUPS\Backups"
$rest_path = "C:\VERTEC_BACKUPS\Restores"

################################################################

if (-Not $args[0]) {
    $latest_file = Dir $bkup_path | Sort CreationTime -Descending | Select Name -First 1
    $bkup_file = $bkup_path + "\" + $latest_file.Name
    Write-Host "No backup filen specified, using latest available"
}
else {
    $bkup_file = $args[0]
}

if (-Not (Test-Path $bkup_file)) {
    Write-Host "File not found: $bkup_file"
    exit 1
}

$rest_db = $fb_host + ":" + $rest_path + "\" + ((Split-Path $bkup_file -leaf) -replace ".FBK") + ".FDB"
Write-Host "Backup file: $bkup_file"
Write-Host "Restore database: $rest_db"

$fb_gbak_args = @(
    "-CREATE_DATABASE",
    "-USER", $fb_user,
    "-PASSWORD", $fb_pass,
    $bkup_file,
    $rest_db
)

& $fb_gbak $fb_gbak_args
$gbak_success = $?
If($gbak_success) {
    Write-Host "Restore was SUCCESSFUL"
}
else {
    Write-Host "Restore has FAILED"
}

# eof