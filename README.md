# README #

### What is this repository for? ###

Vertec backup and restore scripts

* Hot backup of Firebird / Vertec databases
* Backups are only kept for 120 days (configurable)
* Email notification for backup
* Restore script

### Requirements ###

* Vertec
* Firebird
* Powershell

### Installation ###

1. Create C:\VERTEC_BACKUPS\Backups
2. Create C:\VERTEC_BACKUPS\Restores 
3. Copy the files to C:\VERTEC_BACKUPS
4. Edit both files, settings at the top
5. Create a planned task (execute all 3 hours)
6. Monitor content of email

### Usage ###

VERTEC_Backup.ps1 - No parameters

VERTEC_Restore.ps1 - specify file name to restore, without, it's the latest
