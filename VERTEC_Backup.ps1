################################################################
#
# Vertec Database Dump Script
#
################################################################

$email_from = "mail@domain.tld"
$email_to = "mail@domain.tld"
$email_subj = "Vertec PROD Database Backup"

$smtp_server = "localhost"
$smtp_user = "yourusername"
$smtp_pass = "yourpassword"

$fb_gbak = "C:\VERTEC\Database\gbak.exe"
$fb_user = "SYSDBA"
$fb_pass = "masterkey"
$fb_srcdb = "localhost:C:\VERTEC\DB\VERTEC_DB.fdb"

$bkup_keep = -120
$bkup_name = "VERTEC_BACKUP"
$bkup_path = "C:\VERTEC_BACKUPS\Backups"
$bkup_logs = "C:\VERTEC_BACKUPS\GBAK_Log.txt"

################################################################

if (Test-Path $bkup_logs) {
  Remove-Item $bkup_logs
}

$datenow = Get-Date
$datetodelete = $datenow.AddDays($bkup_keep)
$bkup_file = $bkup_path + "\" + $bkup_name + "-" + $datenow.ToString("yyyyMMdd-HHmm") + ".FBK"

$fb_gbak_args = @(
    "-BACKUP_DATABASE",
    "-GARBAGE_COLLECT",
    "-USER", $fb_user,
    "-PASSWORD", $fb_pass,
    "-Z",
    "-Y", $bkup_logs,
    $fb_srcdb,
    $bkup_file
)

& $fb_gbak $fb_gbak_args
$gbak_success = $?

Add-Content $bkup_logs "$datenow"
Add-Content $bkup_logs "$fb_srcdb ---> $bkup_file"
If($gbak_success) { Add-Content $bkup_logs "Firebird Database Backup was SUCCESSFUL" }
Else { Add-Content $bkuplogs "Firebird Database Backup has FAILED" }

Get-ChildItem $bkup_path | Where-Object { $_.LastWriteTime -lt $datetodelete } | Remove-Item

$mailbody = Get-Content -Path $bkup_logs
$mailbody = [string]::join("<br/>", $mailbody)

$msg = new-object Net.Mail.MailMessage
$msg.From = $email_from
$msg.To.Add($email_to)
$msg.BodyEncoding = [system.Text.Encoding]::Unicode
$msg.SubjectEncoding = [system.Text.Encoding]::Unicode
$msg.IsBodyHTML = $true
$msg.Subject = $email_subj
$msg.Body = $mailbody

$smtp = new-object Net.Mail.SmtpClient($smtp_server)
$smtp.EnableSsl = $true
$smtp.Credentials = New-Object System.Net.NetworkCredential($smtp_user, $smtp_pass);
$smtp.Send($msg)

# eof